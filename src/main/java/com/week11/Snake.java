package com.week11;

public class Snake extends Animal implements Crawable{

    public Snake(String name) {
        super(name, 0);
    }

    @Override
    public void eat() {
        System.out.println(toString() + " eat. ");
    }

    @Override
    public void sleep() {
        System.out.println(toString() + " sleep. ");
    }

    @Override
    public String toString() {
        return "Snake(" + this.getName() + ")";
    }

    @Override
    public void craw() {
        System.out.println(this + " craw.");
        
    }
}

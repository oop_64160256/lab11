package com.week11;

public class Dog extends Landanimal {

    public Dog(String name) {
        super(name, 4);

    }

    @Override
    public void eat() {
        System.out.println(toString() + " eat. ");
    }

    @Override
    public void sleep() {
        System.out.println(toString() + " sleep. ");
    }

    @Override
    public String toString() {
        return "Dog(" + this.getName() + ")";
    }

}

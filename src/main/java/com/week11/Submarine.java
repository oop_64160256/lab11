package com.week11;

public class Submarine extends Vahicle implements Swimable{

    Submarine(String name, String engineName) {
        super(name, engineName);
    }

    @Override
    public void swim() {
        System.out.println(this + " swim.");
    }

}

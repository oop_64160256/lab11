package com.week11;

public class App {
    public static void main(String[] args) {
        Dog dog1 = new Dog("Scoop");

        Human human1 = new Human("Keng");

        Cat cat1 = new Cat("Garfield");

        Rat rat1 = new Rat("Remy");

        Bird bird1 = new Bird("Dumbo");

        Fish fish1 = new Fish("Marine");

        Bat bat1 = new Bat("wayne");

        Snake snake1 = new Snake("Orochimaru");

        Crocodile crocodile1 = new Crocodile("Charawan");

        Plane plane1 = new Plane("Boing", "Boing-123");

        Submarine submarine1 = new Submarine("Yuan-class S26T", "Yuan-class's Engine");



        Flyable[] flyableobject = { bird1, bat1, plane1 };
        System.out.println("----------Flyable----------");
        for (int i = 0; i < flyableobject.length; i++) {
            flyableobject[i].fly();
            flyableobject[i].takeoff();
            flyableobject[i].landing();
        }
        System.out.println();

        Swimable[] swimableobject = { submarine1, fish1 , crocodile1};
        System.out.println("----------Swimable----------");
        for (int i = 0; i < swimableobject.length; i++) {
            swimableobject[i].swim();
        }
        System.out.println();

        Crawable[] crawableobject = { snake1, crocodile1};
        System.out.println("----------Crawable----------");
        for (int i = 0; i < crawableobject.length; i++) {
            crawableobject[i].craw();
        }
        System.out.println();

        Landanimal[] landanimalobject = { human1, cat1, dog1, rat1};
        System.out.println("----------Landanimal----------");
        for (int i = 0; i < landanimalobject.length; i++) {
            landanimalobject[i].walk();
            landanimalobject[i].run();
        }
        System.out.println();
    }
}

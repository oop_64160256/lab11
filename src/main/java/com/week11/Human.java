package com.week11;

public class Human extends Landanimal{

    public Human(String name) {
        super(name, 2);
    }

    @Override
    public void eat() {
        System.out.println(toString()+" eat. ");
    }

    @Override
    public void sleep() {
        System.out.println(toString()+" sleep. ");
    }

    @Override
    public String toString() {
        return "Human(" + this.getName() + ")";
    }
    
}

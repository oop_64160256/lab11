package com.week11;

public class Landanimal extends Animal{

    public Landanimal(String name, int numOfLeg) {
        super(name, numOfLeg);
    }

    @Override
    public void eat() {
        System.out.println(toString()+" eat. ");
    }

    @Override
    public void sleep() {
        System.out.println(toString()+" sleep. ");
    }

    public void walk() {
        System.out.println(this + " walk.");
    }
    
    public void run() {
        System.out.println(this + " run.");
    }
}

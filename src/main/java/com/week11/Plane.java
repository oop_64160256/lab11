package com.week11;

public class Plane extends Vahicle implements Flyable {

    Plane(String name, String engineName) {
        super(name, engineName);
    }

    @Override
    public void fly() {
        System.out.println(this + " fly. ");
    }

    @Override
    public void takeoff() {
        System.out.println(this + " takeoff. ");

    }

    @Override
    public void landing() {
        System.out.println(this + " landing. ");

    }
}

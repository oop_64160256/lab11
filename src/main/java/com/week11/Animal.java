package com.week11;

public abstract class Animal {
    private String name;
    private int numOfLeg;

    public Animal(String name, int numOfLeg) {
        this.name = name;
        this.numOfLeg = numOfLeg;
    }

    public String getName() {
        return name;
    }

    public int getNumOfLeg() {
        return numOfLeg;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumOfLeg(int numOfLeg) {
        this.numOfLeg = numOfLeg;
    }

    public abstract void eat();

    public abstract void sleep();

    @Override
    public String toString() {
        return "Animal (" + name + ") has " + numOfLeg + " legs";
    }
}
